
#include "A4Types.h"
#include <iostream>

Money::Money(){
    numDollars = 0;
    numQuarters = 0;
    numDimes = 0;
    numNickels = 0;
    numPennies = 0;
}

Money::Money(unsigned int dollars, unsigned int cents){
    numDollars = dollars;
    numQuarters = 0;
    numDimes = 0;
    numNickels = 0;
    numPennies = cents;

}

Money::Money(unsigned int dd, unsigned int q, unsigned int d, unsigned int n, unsigned int p){
    numDollars = dd;
    numQuarters = q;
    numDimes = d;
    numNickels = n;
    numPennies = p;
}

unsigned int Money::getDollars(){
    return numDollars;
}
unsigned int Money::getQuarters(){
     return numQuarters;
}
unsigned int Money::getDimes(){
      return numDimes;
}
unsigned int Money::getNickels(){
      return numNickels;
}
unsigned int Money::getPennies(){
      return numPennies;
}

unsigned int Money::getCents(){
    unsigned int cents,quarters,nickels,pennies;
    quarters = getQuarters();
    nickels = getNickels();
    pennies = getPennies();

    cents = ((quarters*25)+(nickels*5)+pennies);
    return(cents);
}

// setter functions
void Money::addMoney(Money m){
    numDimes+= m.getDimes();
    numDollars+=m.getDollars();
    numQuarters+=m.getQuarters();
    numNickels+=m.getNickels();
    numPennies+=m.getPennies();
}
void Money::addDollars(unsigned int d){
    numDollars+=d;
}
void Money::addQuarters(unsigned int q){
    numQuarters+=q;
}
void Money::addDimes(unsigned int d){
    numDimes+=d;
}
void Money::addNickels(unsigned int n){
    numNickels+=n;
}
void Money::addPennies(unsigned int p){
    numPennies+=p;
}
void Money::addCents(unsigned int c){
    unsigned int cent;
    cent+=c;
}

void         Money::leastCoins(){

}
unsigned int Money::numberOfCoins(){


}
Money* makeChange(const Money& cost, const Money& paid);
Money* makeChangeNpPennies(const Money& cost, const Money& paid);




